#!perl
use diagnostics;
use Env qw(HOME);
use strict;
use vars qw/$pdf/;
use warnings;

use HTML::Tiny;
use Digest::SHA1 qw/sha1_hex/;
use File::Copy;

my $h = HTML::Tiny->new;
open(my $html, '>', "$HOME/public_html/papers/index.html");
print $html '<html><head><title>Papers Hosted here</title></head><body>';
opendir(my $dh, "$HOME/public_html/papers");
chdir "$HOME/public_html/papers";
foreach my $file (readdir($dh)) {
    if ($file =~ /\.pdf$/) {
        open $pdf, "<$HOME/public_html/papers/$file" or die "$!";
        my $filecontent = '';
        $filecontent .= "$_\n" foreach(<$pdf>);
        close $pdf;
        my $new_name = sha1_hex($filecontent);
        rename($file, $new_name);
        `git add ${new_name}.pdf`;
    }
}
print $html '</body></html>';
close $html;
