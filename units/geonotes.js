function dataCallback(data) {
    if (data.length > 0) {
        $("#notecount").text(data.length+" "); 
        if ($("#notecount").text == " ") { 
            $("#notecount").text("No "); 
        }
        $.each(data, function(k, v) {
            $.getJSON('//hd1-atlas.herokuapp.com/info', {'olng': pos.coords.longitude, 'olat': pos.coords.latitude, 'lng': v.longitude, 'lat': v.latitude}).done(function(d) {
                var opts = '';
                if($('#icon').val() != '') {
                    opts = L.AwesomeMarker.icon({icon: $('icon').text(), prefix: 'fa', markerColor: 'blue'})
                } else {
                    opts = L.AwesomeMarker.icon({markerColor: 'blue'})
                }
                L.marker([v.latitude, v.longitude], {iconSize: [16,16], icon: opts, opacity: 0.75, }).addTo(mymap).bindPopup(function(event) {
                    console.log('table row should be {'+v.note+', '+v.created_at+', '+d.distance+', '+d.bearing+'}');
                    var line = "<tr><td>"+v.note+"</td><td>"+v.created_at+"</td><td>"+d.distance+"</td><td>"+d.bearing+"</td></tr>";
                    $(document).one(Notification.requestPermission().then(function(result) {
                        var n = new Notification(v.note+' created, '+d.distance+' meters at a bearing of '+d.bearing);
                    }));
                    $("tbody").append(line);
                    $('table').show();
                }, {'maxWidth': 0, 'maxHeight': 0, 'minWidth': 0, closeButton: false });
            });
        });
    }
    $("table").show();
}
$(document).ready(function() {
    // only show table if there are notes
    $('table').hide();
    // only show submit on touch devices
    $('#submit').hide();
    window.addEventListener('touchstart', function onFirstTouch() {
        $('#submit').show();
    });
    setInterval(navigator.geolocation.getCurrentPosition(function(pos) {
        var mymap = L.map('map').setView([pos.coords.latitude, pos.coords.longitude], 9);
        L.tileLayer('//server.arcgisonline.com/ArcGIS/rest/services/Specialty/DeLorme_World_Base_Map/MapServer/tile/{z}/{y}/{x}', { attribution: '', maxZoom: 11, minZoom: 1}).addTo(mymap);
        var redMarker = L.AwesomeMarkers.icon({ icon: 'home',  prefix: 'fa', markerColor: 'red' });
        var currentLocationMarker = L.marker([pos.coords.latitude, pos.coords.longitude], {iconSize: [16,16], icon: redMarker, opacity: 0.75,});
        $('.form-control').mouseenter(function() { currentLocationMarker.addTo(mymap); }).mouseleave(function() { mymap.removeLayer(currentLocationMarker); });
        $('form').submit(function(e) { 
            e.preventDefault(); 
            var form = new FormData();
            form.append("latitude", pos.coords.latitude);
            form.append("longitude", pos.coords.longitude);
            form.append("note", $('#content').val());

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "//hd1-geonotes.herokuapp.com/places.json",
                "method": "POST",
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form,
            }

            $.ajax(settings).done(function (response) {
                $.notify({
                    // options
                    message: 'Added "'+$('#content').val()+'"', 
                },{
                    // settings
                    type: 'danger'
                });
                
                data = $.getJSON('//hd1-geonotes.herokuapp.com/places.json').success(function() { dataCallback(data.responseJSON);});
        })});
        $('#map').mouseenter(function() { mymap.dragging.disable(); }).mouseleave(function() { mymap.dragging.enable(); });
        data = $.getJSON('//hd1-geonotes.herokuapp.com/places.json');
        dataCallback(data.responseJSON);
    }), 45000);
